package com.danf.dsproject.grpc;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.15.0)",
    comments = "Source: service.proto")
public final class medical_planGrpc {

  private medical_planGrpc() {}

  public static final String SERVICE_NAME = "medical_plan";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<com.danf.dsproject.grpc.Service.DataRequest,
      com.danf.dsproject.grpc.Service.Empty> getSendDataMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "send_data",
      requestType = com.danf.dsproject.grpc.Service.DataRequest.class,
      responseType = com.danf.dsproject.grpc.Service.Empty.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.danf.dsproject.grpc.Service.DataRequest,
      com.danf.dsproject.grpc.Service.Empty> getSendDataMethod() {
    io.grpc.MethodDescriptor<com.danf.dsproject.grpc.Service.DataRequest, com.danf.dsproject.grpc.Service.Empty> getSendDataMethod;
    if ((getSendDataMethod = medical_planGrpc.getSendDataMethod) == null) {
      synchronized (medical_planGrpc.class) {
        if ((getSendDataMethod = medical_planGrpc.getSendDataMethod) == null) {
          medical_planGrpc.getSendDataMethod = getSendDataMethod = 
              io.grpc.MethodDescriptor.<com.danf.dsproject.grpc.Service.DataRequest, com.danf.dsproject.grpc.Service.Empty>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "medical_plan", "send_data"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.danf.dsproject.grpc.Service.DataRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.danf.dsproject.grpc.Service.Empty.getDefaultInstance()))
                  .setSchemaDescriptor(new medical_planMethodDescriptorSupplier("send_data"))
                  .build();
          }
        }
     }
     return getSendDataMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.danf.dsproject.grpc.Service.DataRequest,
      com.danf.dsproject.grpc.Service.Response> getReceiveDataMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "receive_data",
      requestType = com.danf.dsproject.grpc.Service.DataRequest.class,
      responseType = com.danf.dsproject.grpc.Service.Response.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.danf.dsproject.grpc.Service.DataRequest,
      com.danf.dsproject.grpc.Service.Response> getReceiveDataMethod() {
    io.grpc.MethodDescriptor<com.danf.dsproject.grpc.Service.DataRequest, com.danf.dsproject.grpc.Service.Response> getReceiveDataMethod;
    if ((getReceiveDataMethod = medical_planGrpc.getReceiveDataMethod) == null) {
      synchronized (medical_planGrpc.class) {
        if ((getReceiveDataMethod = medical_planGrpc.getReceiveDataMethod) == null) {
          medical_planGrpc.getReceiveDataMethod = getReceiveDataMethod = 
              io.grpc.MethodDescriptor.<com.danf.dsproject.grpc.Service.DataRequest, com.danf.dsproject.grpc.Service.Response>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "medical_plan", "receive_data"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.danf.dsproject.grpc.Service.DataRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.danf.dsproject.grpc.Service.Response.getDefaultInstance()))
                  .setSchemaDescriptor(new medical_planMethodDescriptorSupplier("receive_data"))
                  .build();
          }
        }
     }
     return getReceiveDataMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static medical_planStub newStub(io.grpc.Channel channel) {
    return new medical_planStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static medical_planBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new medical_planBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static medical_planFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new medical_planFutureStub(channel);
  }

  /**
   */
  public static abstract class medical_planImplBase implements io.grpc.BindableService {

    /**
     */
    public void sendData(com.danf.dsproject.grpc.Service.DataRequest request,
        io.grpc.stub.StreamObserver<com.danf.dsproject.grpc.Service.Empty> responseObserver) {
      asyncUnimplementedUnaryCall(getSendDataMethod(), responseObserver);
    }

    /**
     */
    public void receiveData(com.danf.dsproject.grpc.Service.DataRequest request,
        io.grpc.stub.StreamObserver<com.danf.dsproject.grpc.Service.Response> responseObserver) {
      asyncUnimplementedUnaryCall(getReceiveDataMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getSendDataMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.danf.dsproject.grpc.Service.DataRequest,
                com.danf.dsproject.grpc.Service.Empty>(
                  this, METHODID_SEND_DATA)))
          .addMethod(
            getReceiveDataMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.danf.dsproject.grpc.Service.DataRequest,
                com.danf.dsproject.grpc.Service.Response>(
                  this, METHODID_RECEIVE_DATA)))
          .build();
    }
  }

  /**
   */
  public static final class medical_planStub extends io.grpc.stub.AbstractStub<medical_planStub> {
    private medical_planStub(io.grpc.Channel channel) {
      super(channel);
    }

    private medical_planStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected medical_planStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new medical_planStub(channel, callOptions);
    }

    /**
     */
    public void sendData(com.danf.dsproject.grpc.Service.DataRequest request,
        io.grpc.stub.StreamObserver<com.danf.dsproject.grpc.Service.Empty> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getSendDataMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void receiveData(com.danf.dsproject.grpc.Service.DataRequest request,
        io.grpc.stub.StreamObserver<com.danf.dsproject.grpc.Service.Response> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getReceiveDataMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class medical_planBlockingStub extends io.grpc.stub.AbstractStub<medical_planBlockingStub> {
    private medical_planBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private medical_planBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected medical_planBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new medical_planBlockingStub(channel, callOptions);
    }

    /**
     */
    public com.danf.dsproject.grpc.Service.Empty sendData(com.danf.dsproject.grpc.Service.DataRequest request) {
      return blockingUnaryCall(
          getChannel(), getSendDataMethod(), getCallOptions(), request);
    }

    /**
     */
    public com.danf.dsproject.grpc.Service.Response receiveData(com.danf.dsproject.grpc.Service.DataRequest request) {
      return blockingUnaryCall(
          getChannel(), getReceiveDataMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class medical_planFutureStub extends io.grpc.stub.AbstractStub<medical_planFutureStub> {
    private medical_planFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private medical_planFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected medical_planFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new medical_planFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.danf.dsproject.grpc.Service.Empty> sendData(
        com.danf.dsproject.grpc.Service.DataRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getSendDataMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.danf.dsproject.grpc.Service.Response> receiveData(
        com.danf.dsproject.grpc.Service.DataRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getReceiveDataMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_SEND_DATA = 0;
  private static final int METHODID_RECEIVE_DATA = 1;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final medical_planImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(medical_planImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_SEND_DATA:
          serviceImpl.sendData((com.danf.dsproject.grpc.Service.DataRequest) request,
              (io.grpc.stub.StreamObserver<com.danf.dsproject.grpc.Service.Empty>) responseObserver);
          break;
        case METHODID_RECEIVE_DATA:
          serviceImpl.receiveData((com.danf.dsproject.grpc.Service.DataRequest) request,
              (io.grpc.stub.StreamObserver<com.danf.dsproject.grpc.Service.Response>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class medical_planBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    medical_planBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return com.danf.dsproject.grpc.Service.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("medical_plan");
    }
  }

  private static final class medical_planFileDescriptorSupplier
      extends medical_planBaseDescriptorSupplier {
    medical_planFileDescriptorSupplier() {}
  }

  private static final class medical_planMethodDescriptorSupplier
      extends medical_planBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    medical_planMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (medical_planGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new medical_planFileDescriptorSupplier())
              .addMethod(getSendDataMethod())
              .addMethod(getReceiveDataMethod())
              .build();
        }
      }
    }
    return result;
  }
}
