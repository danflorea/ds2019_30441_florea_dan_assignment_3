package com.danf.hahayes.GrpcClientController;

import com.danf.dsproject.grpc.Service;
import com.danf.dsproject.grpc.medical_planGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import javax.swing.*;
import javax.xml.crypto.Data;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;

import static com.danf.dsproject.grpc.medical_planGrpc.newBlockingStub;

@Component
public class GrpcClient implements CommandLineRunner {


    @Override
    public void run(String... args) throws Exception {



        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 9090)
                .usePlaintext()
                .build();

        medical_planGrpc.medical_planBlockingStub stub = medical_planGrpc.newBlockingStub(channel);

        Service.DataRequest dataRequest = Service.DataRequest.newBuilder().setData("Received data").build();

        String data = stub.receiveData(dataRequest).getData();

        System.out.println(data);




        JFrame frame = new JFrame("Medical app");
        frame.setSize(500, 400);
        frame.setLayout(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();

        constraints.gridy = 0;
        constraints.gridx = 0;
        frame.getContentPane().add(new JLabel("Current time"), constraints);

        constraints.gridy = 1;
        constraints.gridx = 0;
        JLabel currentTime = new JLabel();
        frame.getContentPane().add(currentTime, constraints);

        Timer t = new javax.swing.Timer(1000, actionEvent -> {
            currentTime.setText(LocalDateTime.now().toLocalTime().toString());
            frame.revalidate();
            frame.repaint();
        });
        t.start();

        String currentDate = LocalDate.now().toString();
        String[] splitMedPlan = data.split(",");
        java.util.List<String> medPlanToDisplay = new ArrayList<>();
        for (int i = 0; i < splitMedPlan.length-1; i++)
        {
            String[] cplan = splitMedPlan[i].split("/");
            String temp = cplan[2].replaceAll("\\s+","");
            if (currentDate.equals(temp))
            {
                System.out.println("\t\t\tadded");
                String mp = "";
                mp = mp.concat(cplan[0] + " | ");
                mp = mp.concat(" Dosage: " + cplan[1] + " | ");
                mp = mp.concat(cplan[2] + " | ");
                mp = mp.concat(" Instructions: " + cplan[3] + " | ");
                medPlanToDisplay.add(mp);
            }
        }

        constraints.gridy = 2;
        constraints.gridx = 0;
        frame.getContentPane().add(new JLabel(" ____________________________________ "), constraints);

        for (int i = 0; i < medPlanToDisplay.size(); i++) {
            constraints.gridy = i+3;
            constraints.gridx = 0;
            frame.getContentPane().add(new JLabel(medPlanToDisplay.get(i)), constraints);

        }


        constraints.gridy = 4;
        constraints.gridx = 0;
        frame.getContentPane().add(new JLabel(" ____________________________________ "), constraints);
//        frame.getContentPane().add(new JLabel(splitMedPlan[1]), constraints);
//
//        constraints.gridy = 5;
//        constraints.gridx = 0;
//        frame.getContentPane().add(new JLabel(splitMedPlan[2]), constraints);

        constraints.gridy = 6;
        constraints.gridx = 0;
        JButton medicationTakenButton = new JButton("Medication taken");
        medicationTakenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Sending medication taken");
                //TODO
            }
        });
        frame.getContentPane().add(medicationTakenButton, constraints);


        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);


    }
}
