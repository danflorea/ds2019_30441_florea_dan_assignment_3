package com.danf.hahayes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class HahayesApplication {

	public static void main(String[] args) {
//		SpringApplication.run(HahayesApplication.class, args);

		SpringApplicationBuilder builder = new SpringApplicationBuilder(HahayesApplication.class);
		builder.headless(false);
		ConfigurableApplicationContext context = builder.run(args);

	}

}
