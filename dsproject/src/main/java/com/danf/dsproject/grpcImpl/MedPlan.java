package com.danf.dsproject.grpcImpl;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class MedPlan {

    private String medication;
    private String dosage;
    private LocalDate medicationDate;
    private String instructions;

    public String toString(){
        return medication + " / " +
                dosage + " / " +
                medicationDate + " / " +
                instructions + " , ";
    }


}
