package com.danf.dsproject.grpcImpl;

import com.danf.dsproject.grpc.Service;
import com.danf.dsproject.grpc.medical_planGrpc;
import io.grpc.stub.StreamObserver;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class MedicationPlanService extends medical_planGrpc.medical_planImplBase {

    @Override
    public void sendData(Service.DataRequest request, StreamObserver<Service.Empty> responseObserver) {
        super.sendData(request, responseObserver);
    }

    @Override
    public void receiveData(Service.DataRequest request, StreamObserver<Service.Response> responseObserver) {
        String data = request.getData();
        System.out.println(data);
        Service.Response.Builder response = Service.Response.newBuilder();

        LocalDate date1 = LocalDate.parse("2019-11-20");
        LocalDate date2 = LocalDate.parse("2019-11-27");
//        System.out.println(date1 + " - " + date2);

        MedPlan medPlan = new MedPlan("Paracetamol", "1000 mg", date1, "Orally administered; once per day");
        MedPlan medPlan1 = new MedPlan("Cough syrup", "100 ml", date2, "Orally administered; Twice a day");
        MedPlan medPlan2 = new MedPlan("Syrup", "100 ml", date2, "Orally administered; Twice a day");

        List<MedPlan> plans = new ArrayList<>();
        plans.add(medPlan);
        plans.add(medPlan1);
        plans.add(medPlan2);


//        String plan = "MEDICATION:\t Paracetamol - DOSAGE:\t 1000 mg - INSTRUCTIONS:\t 500 mg in the morning, 500 mg in the evening";
        String msg = "";
        for(MedPlan p: plans){
            String pts = p.toString();
            msg = msg.concat(pts);
        }

        response.setData(msg);
        responseObserver.onNext(response.build());
        responseObserver.onCompleted();

    }
}