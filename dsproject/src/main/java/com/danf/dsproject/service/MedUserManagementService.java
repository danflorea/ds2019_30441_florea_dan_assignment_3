package com.danf.dsproject.service;

import com.danf.dsproject.dto.CompleteMedUserDTO;
import com.danf.dsproject.dto.MedUserDTO;
import com.danf.dsproject.entities.MedUser;
import com.danf.dsproject.persistence.jpa.JpaRepositoryFactory;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class MedUserManagementService {

    private final JpaRepositoryFactory factory;

    public void delete (MedUser user) {
        factory.createMedUserRepository().remove(user);
    }

    public Optional<MedUser> findUserById (Integer id) {
        return this.factory.createMedUserRepository().findById(id);
    }

    public List<MedUser> findAll () {
        return this.factory.createMedUserRepository().findAll();
    }

    public MedUser save (MedUser user) {
        return this.factory.createMedUserRepository().save(user);
    }

    public List<MedUserDTO> getPatientsOfCaregiver (Integer id)
    {
        List<MedUserDTO> patients = new ArrayList<>();
        Optional<MedUser> mu = this.factory.createMedUserRepository().findById(id);
        if (mu.isPresent())
        {
            List<MedUser> hahayes = new ArrayList<>();
            hahayes.add(mu.get());
            patients = this.factory.createMedUserRepository().findMedUsersByCaregivers(hahayes).stream().map(patient -> MedUserDTO.ofEntity(patient)).collect(Collectors.toList());

        }
        return patients;
    }
    
}
