package com.danf.dsproject.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReducedMedicationInfoDTO {

    private Integer mid;
    private String name;

}
