package com.danf.dsproject.persistence.api;


import com.danf.dsproject.receiver.PatientData;

import java.util.List;
import java.util.Optional;

public interface PatientDataRepository {

    PatientData save (PatientData patientData);
    Optional<PatientData> findById(Integer id);
    List<PatientData> findAll();
    void remove(PatientData patientData);

}
