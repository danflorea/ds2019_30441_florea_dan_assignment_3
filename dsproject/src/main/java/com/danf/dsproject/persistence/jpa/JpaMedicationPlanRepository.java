package com.danf.dsproject.persistence.jpa;

import com.danf.dsproject.entities.MedicationPlan;
import com.danf.dsproject.persistence.api.MedicationPlanRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.Repository;

public interface JpaMedicationPlanRepository extends Repository<MedicationPlan, Integer>, MedicationPlanRepository {

    void delete(MedicationPlan element);

    @Override
    default void remove(MedicationPlan element){
        delete(element);
    }
}
